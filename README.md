# Projeto de Gerenciamento de Funcionários
Teste para Avaliação de Conhecimento utilizando AngularJS e Node.js.

## Funcionalidades

- **Listagem de Colaboradores**: Todos os colaboradores são exibidos numa tabela paginada. Acesso via `GET` em `/colaboradores?_page=1&_per_page=10`.
- **Pesquisa de Colaboradores**: Busque colaboradores por nome com atualização em tempo real. Acesso via `GET` em `/colaboradores?nome_like=seuNomeAqui`.
- **Criação de Colaboradores**: Adicione novos colaboradores com um formulário interativo. Acesso via `POST` em `/colaboradores`.
- **Edição de Colaboradores**: Atualize dados dos colaboradores com facilidade. Acesso via `PUT` em `/colaboradores/:id`.
- **Exclusão de Colaboradores**: Remova colaboradores diretamente. Acesso via `DELETE` em `/colaboradores/:id`.


## Tecnologias Utilizadas

- **Front-end**: AngularJS e Bootstrap.
- **Ferramentas Auxiliares**:
  - UI Utils Masks e NG Mask para validação de dados.
  - Lite Server para servidor local.
  - Prettier para formatação de código.
  - Karma e Jasmine para testes unitários.

## Setup do Projeto

Para executar este projeto localmente, siga os passos abaixo:

1. Clone o repositório.
2. Utilize `nvm` para configurar a versão do Node.js recomendada (`v20.13.0`).
3. Execute `npm install` para instalar as dependências.
4. Inicie o servidor com `npm start`.


## Estrutura do Projeto

O projeto é dividido em várias partes principais:

- **Services**: Gerenciam as chamadas de API.
- **Controllers**: Controlam a lógica entre a interface e os dados.
- **Views**: Responsáveis pela apresentação visual e interações do usuário.

## Testes

O projeto inclui testes unitários para assegurar a qualidade. Execute-os com `npm test`.


## Fala aí!
Espero que curtam o projeto e vejam como posso contribuir para a equipe. Estou aberto a sugestões e melhorias!
